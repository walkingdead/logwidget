How to start application.

Basically you have two options 

 1) Open your project inside
 IDE and launch com.log.widget.LogWidgetApplication and specify VM options
 parameter -Dspring.profiles.active=dev

 2) - execute command ./gradlew build
    - launch application with following command
    `java -Dspring.profiles.active=dev -jar /build/libs/logWidget-1.0-SNAPSHOT.jar`
    or if you want to customize logfile path or server port you ca use
    `SERVER_PORT=8090 CONFIG_LOGFILEPATH=/root/logs.log CONFIG_GENARATION_PERIOD=2000 java -Dspring.profiles.active=dev -jar build/libs/logWidget-1.0-SNAPSHOT.jar`
    Note: CONFIG_GENARATION_PERIOD is measured in ms.