package com.log.widget.unit.service;

import com.log.widget.common.annotation.UnitTestRunner;
import com.log.widget.common.utils.TestUtils;
import com.log.widget.dto.LoggingGeneratorStatus;
import com.log.widget.props.GeneralProperties;
import com.log.widget.service.LogEventParser;
import com.log.widget.service.LogFileService;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@UnitTestRunner
public class LogFileServiceTest {

    private LogFileService logFileService;

    @Before
    public void init(){
        GeneralProperties generalProperties = new GeneralProperties();
        generalProperties.setLogfilePath(TestUtils.TEST_LOG_FILE);
        generalProperties.setGenerationPeriod(1000);
        this.logFileService = new LogFileService(generalProperties, new LogEventParser());
    }

    @Test
    public void startStopLogging(){
        assertThat("Logging service stopped", this.logFileService.getStatus(),is(LoggingGeneratorStatus.STOPPED));

        this.logFileService.startLogging();

        assertThat("Logging service started", this.logFileService.getStatus(),is(LoggingGeneratorStatus.STARTED));

        this.logFileService.stopLogging();

        assertThat("Logging service stopped", this.logFileService.getStatus(),is(LoggingGeneratorStatus.STOPPED));
    }
}
