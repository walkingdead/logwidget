package com.log.widget.unit.service;


import com.log.widget.model.LogEvent;
import com.log.widget.service.LogEventParser;
import com.log.widget.utils.DateTimeUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.format.DateTimeParseException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LogEventParserTest {

    private LogEventParser logEventParser = new LogEventParser();

    @Rule
    public ExpectedException expectException = ExpectedException.none();

    @Test
    public void successfullyParseLogLine() {
        String logEventLine = "2017-06-11 05:01:47,821 ERROR Some error message";
        Optional<LogEvent> logEventOptional = logEventParser.parse(logEventLine);
        assertThat("Log Event is present", logEventOptional.isPresent(), is(true));
        LogEvent logEvent = logEventOptional.get();
        assertThat("Log Event date time", logEvent.getDateTime(), is(DateTimeUtils.parseDateTime("2017-06-11 05:01:47,821")));
        assertThat("Log Event severity", logEvent.getSeverity(), is("ERROR"));
        assertThat("Log Event message", logEvent.getMessage(), is("Some error message"));
    }

    @Test
    public void failToParseDateTime() {
        String logEventLine = "2017-06-1105:01:47,821";
        Optional<LogEvent> logEventOptional = logEventParser.parse(logEventLine);
        assertThat("Log Event is not present", logEventOptional.isPresent(), is(false));

    }

    @Test
    public void failToParseSeverity() {
        String logEventLine = "2017-06-11 05:01:47,821";
        Optional<LogEvent> logEventOptional = logEventParser.parse(logEventLine);
        assertThat("Log Event is not present", logEventOptional.isPresent(), is(false));
    }

    @Test
    public void failToParseMessage() {
        String logEventLine = "2017-06-11 05:01:47,821 ERROR";
        Optional<LogEvent> logEventOptional = logEventParser.parse(logEventLine);
        assertThat("Log Event is not present", logEventOptional.isPresent(), is(false));
    }

    @Test
    public void failToParseIncorrectDateTime() {
        String logEventLine = "017-06-11 05:01:47,821 ERROR some Message";
        expectException.expect(DateTimeParseException.class);
        expectException.expectMessage("Text '017-06-11 05:01:47,821' could not be parsed at index 0");
        logEventParser.parse(logEventLine);
    }

    @Test
    public void failToParseIncorrectSeverityLevel() {
        String logEventLine = "2017-06-11 05:01:47,821 TRACE some Message";
        Optional<LogEvent> logEventOptional = logEventParser.parse(logEventLine);
        assertThat("Log Event is not present", logEventOptional.isPresent(), is(false));
    }
}
