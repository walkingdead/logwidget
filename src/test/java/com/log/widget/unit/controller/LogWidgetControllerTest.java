package com.log.widget.unit.controller;

import com.google.common.collect.ImmutableMap;
import com.log.widget.common.annotation.UnitTestRunner;
import com.log.widget.common.utils.TestUtils;
import com.log.widget.controller.LogWidgetController;
import com.log.widget.dto.LoggingGeneratorStatus;
import com.log.widget.dto.Statistic;
import com.log.widget.props.GeneralProperties;
import com.log.widget.service.LogService;
import com.log.widget.utils.DateTimeUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(LogWidgetController.class)
@UnitTestRunner
public class LogWidgetControllerTest {

    @MockBean
    private LogService logService;

    @Autowired
    private GeneralProperties generalProperties;


    @Autowired
    private MockMvc mockMvc;

    @BeforeClass
    public static void init() {
        TestUtils.deleteTestLogFile();
    }

    @AfterClass
    public static void destroy() {
        TestUtils.deleteTestLogFile();
    }

    @Test
    public void startLogging() throws Exception {
        when(logService.getStatus()).thenReturn(LoggingGeneratorStatus.STARTED);
        this.mockMvc.perform(put("/start"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(
                        "{\"status\":\"STARTED\"}"
                ));
    }

    @Test
    public void stopLogging() throws Exception {
        when(logService.getStatus()).thenReturn(LoggingGeneratorStatus.STOPPED);
        this.mockMvc.perform(put("/stop"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(
                        "{\"status\":\"STOPPED\"}"
                ));
    }

    @Test
    public void synchronizeLogging() throws Exception {
        when(logService.getStatus()).thenReturn(LoggingGeneratorStatus.STOPPED);
        this.mockMvc.perform(get("/status"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(
                        "{\"status\":\"STOPPED\"}"
                ));
    }

    @Test
    public void getLastEvents() throws Exception {
        String dateTimeRaw = "2017-06-11 05:01:34,821";
        LocalDateTime dateTime = DateTimeUtils.parseDateTime(dateTimeRaw);
        Map<String, Integer> logStatistic = ImmutableMap.<String, Integer>builder()
                .put("INFO", 1)
                .put("WARNING", 0)
                .put("ERROR", 1)
                .build();

        when(logService.getLastEvents(dateTime, 5)).thenReturn(new Statistic(logStatistic, LoggingGeneratorStatus.STARTED));
        this.mockMvc.perform(get("/statistic")
                .param("endDateTime", dateTimeRaw)
                .param("period", "5"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(
                        "{\"logStatistic\":{\"INFO\":1,\"WARNING\":0,\"ERROR\":1},\"status\":{\"status\":\"STARTED\"}}"
                ));
    }

    @Test
    public void getLastEventsIncorrectDateTimeParameter() throws Exception {
        this.mockMvc.perform(get("/statistic")
                .param("endDateTime", "017-05-12")
                .param("period", "5"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(
                        "{\"message\":\"Incorrect value [017-05-12] for parameter 'endDateTime'\"}"
                ));
    }

    @Test
    public void getLastEventsIncorrectPeriodParameter() throws Exception {
        this.mockMvc.perform(get("/statistic")
                .param("endDateTime", "2017-06-11 05:01:34,821")
                .param("period", "abc"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(
                        "{\"message\":\"Incorrect value [abc] for parameter 'period'\"}"
                ));
    }
}
