package com.log.widget.it.controller;

import com.jayway.awaitility.Awaitility;
import com.log.widget.common.annotation.FunctionalTestRunner;
import com.log.widget.common.utils.TestUtils;
import com.log.widget.dto.LoggingGeneratorStatus;
import com.log.widget.dto.Statistic;
import com.log.widget.utils.DateTimeUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class LogWidgetControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${local.server.port}")
    private String port;


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @BeforeClass
    public static void initClass() {
        TestUtils.deleteTestLogFile();
    }

    @AfterClass
    public static void destroyClass() {
        TestUtils.deleteTestLogFile();
    }

    @Before
    public void init() {
        restTemplate.exchange(
                "http://localhost:{port}/stop",
                HttpMethod.PUT, null, new ParameterizedTypeReference<LoggingGeneratorStatus>() {
                }, port);
    }

    @Test
    public void starStopLogging() {
        ResponseEntity<LoggingGeneratorStatus> startReposne = restTemplate.exchange(
                "http://localhost:{port}/start",
                HttpMethod.PUT, null, new ParameterizedTypeReference<LoggingGeneratorStatus>() {
                }, port);

        assertThat("Start logging response code", startReposne.getStatusCode(), is(HttpStatus.OK));
        assertThat("Status must be started", startReposne.getBody(), is(LoggingGeneratorStatus.STARTED));

        ResponseEntity<LoggingGeneratorStatus> stopResponse = restTemplate.exchange(
                "http://localhost:{port}/stop",
                HttpMethod.PUT, null, new ParameterizedTypeReference<LoggingGeneratorStatus>() {
                }, port);

        assertThat("Stop logging response code", stopResponse.getStatusCode(), is(HttpStatus.OK));
        assertThat("Status must be stopped", stopResponse.getBody(), is(LoggingGeneratorStatus.STOPPED));

    }


    @Test
    public void getStatusLogging() {
        restTemplate.exchange(
                "http://localhost:{port}/start",
                HttpMethod.PUT, null, new ParameterizedTypeReference<LoggingGeneratorStatus>() {
                }, port);

        ResponseEntity<LoggingGeneratorStatus> statusResponse = restTemplate.exchange(
                "http://localhost:{port}/status",
                HttpMethod.GET, null, new ParameterizedTypeReference<LoggingGeneratorStatus>() {
                }, port);

        assertThat("Status logging response code", statusResponse.getStatusCode(), is(HttpStatus.OK));
        assertThat("Status must be started", statusResponse.getBody(), is(LoggingGeneratorStatus.STARTED));
    }

    @Test
    public void getLoggingEvents() {
        LocalDateTime localDateTime = LocalDateTime.now(Clock.systemUTC());
        restTemplate.exchange(
                "http://localhost:{port}/start",
                HttpMethod.PUT, null, new ParameterizedTypeReference<LoggingGeneratorStatus>() {
                }, port);

        Awaitility.await().until(() -> LocalDateTime.now(Clock.systemUTC())
                .minus(1, ChronoUnit.SECONDS).isBefore(localDateTime));

        restTemplate.exchange(
                "http://localhost:{port}/stop",
                HttpMethod.PUT, null, new ParameterizedTypeReference<LoggingGeneratorStatus>() {
                }, port);

        String endDateTime = DateTimeUtils.formatDateTime(localDateTime
                .plus(300, ChronoUnit.MILLIS));

        Awaitility.await().until(() -> LocalDateTime.now(Clock.systemUTC())
                .minus(500, ChronoUnit.MILLIS).isBefore(localDateTime));

        ResponseEntity<Statistic> lastEventsResponse = restTemplate.exchange(
                "http://localhost:{port}/statistic?endDateTime={endDateTime}&period={period}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Statistic>() {
                }, port, endDateTime, 1);

        assertThat("Last Events response code", lastEventsResponse.getStatusCode(), is(HttpStatus.OK));

        Statistic statistic = lastEventsResponse.getBody();
        assertThat("Status must be stopped", statistic.getStatus(), is(LoggingGeneratorStatus.STOPPED));

        Integer totalEvents = statistic.getLogStatistic().values().stream()
                .reduce(0, (accumulator, logEvent) -> accumulator + logEvent);
        assertThat("Log file should contain more than 10 events", totalEvents, greaterThan(10));
    }

    @Test
    public void getLoggingEventsIncorrectDateTimeParameter() {
        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        ResponseEntity<Statistic> lastEventsResponse = restTemplate.exchange(
                "http://localhost:{port}/statistic?endDateTime={endDateTime}&period={period}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Statistic>() {
                }, port, "2017-06-05:01:34,821", 1);
    }

    @Test
    public void getLoggingEventsIncorrectPeriodParameter() {
        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        ResponseEntity<Statistic> lastEventsResponse = restTemplate.exchange(
                "http://localhost:{port}/statistic?endDateTime={endDateTime}&period={period}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Statistic>() {
                }, port, "2017-06-05 12:01:34,821", "abdc");
    }
}
