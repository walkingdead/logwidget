package com.log.widget.it.service;

import com.jayway.awaitility.Awaitility;
import com.log.widget.common.annotation.IntegrationTestRunner;
import com.log.widget.common.utils.TestUtils;
import com.log.widget.dto.LoggingGeneratorStatus;
import com.log.widget.dto.Statistic;
import com.log.widget.service.LogService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

@IntegrationTestRunner
@RunWith(SpringJUnit4ClassRunner.class)
public class LogFileServiceTestIT {

    @Autowired
    private LogService logService;

    @BeforeClass
    public static void init() {
        TestUtils.deleteTestLogFile();
    }

    @AfterClass
    public static void destroy() {
        TestUtils.deleteTestLogFile();
    }

    @Test
    public void readEvents() {
        LocalDateTime localDateTime = LocalDateTime.now(Clock.systemUTC());
        this.logService.startLogging();

        assertThat("Logging is started", this.logService.getStatus(), is(LoggingGeneratorStatus.STARTED));

        Awaitility.await().until(() -> LocalDateTime.now(Clock.systemUTC()).minus(200, ChronoUnit.MILLIS).isBefore(localDateTime));
        this.logService.stopLogging();
        Statistic lastEvents = this.logService.getLastEvents(localDateTime.plus(200, ChronoUnit.MILLIS), 1);

        assertThat("Logging is stopped", lastEvents.getStatus(), is(LoggingGeneratorStatus.STOPPED));
        Integer totalEvents = lastEvents.getLogStatistic().values().stream().reduce(0, (accumulator, logEvent) -> accumulator + logEvent);
        assertThat("Log file should contain more than 10 events", totalEvents, greaterThan(10));
    }
}
