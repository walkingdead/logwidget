package com.log.widget.common.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@Profile("test")
@ComponentScan(basePackages = "com.log.widget")
public class IntegrationTestConfiguration {
}
