package com.log.widget.common.utils;

import java.io.File;


public class TestUtils {

    public static final String TEST_LOG_FILE = "src/test/resources/logs.log";

    public static void deleteTestLogFile() {
        File file = new File(TEST_LOG_FILE);
        if (file.exists()) {
            file.delete();
        }
    }
}
