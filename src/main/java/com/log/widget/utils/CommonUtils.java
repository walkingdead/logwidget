package com.log.widget.utils;


import com.log.widget.common.VoidFunction;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;

public class CommonUtils {

    private static final int RANDOM_MULTIPLIER = 1000;

    private CommonUtils() {
        throw new IllegalArgumentException("CommonUtils can't be instantiated");
    }

    public static int getRandomIndex(int start, int end) {
        return ThreadLocalRandom.current().nextInt(start, end * RANDOM_MULTIPLIER + 1) / RANDOM_MULTIPLIER;
    }

    public static void executeWithinLock(Lock lock, VoidFunction function) {
        try {
            lock.lock();
            function.execute();
        } finally {
            lock.unlock();
        }
    }
}
