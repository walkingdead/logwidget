package com.log.widget.utils;


import com.log.widget.common.Constants;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(Constants.DATE_TIME_PATTERN);

    private DateTimeUtils() {
        throw new IllegalArgumentException("DateTimeUtils can't be instantiated");
    }

    public static String formatDateTime(LocalDateTime temporal) {
        return DATE_TIME_FORMATTER.format(temporal);
    }

    public static LocalDateTime parseDateTime(String dateTime) {
        return LocalDateTime.parse(dateTime, DATE_TIME_FORMATTER);
    }

    public static LocalDateTime getLocalDateTimeInUTC(LocalDateTime dateTime) {
        ZonedDateTime ldtZoned = dateTime.atZone(ZoneId.systemDefault());
        ZonedDateTime utcZoned = ldtZoned.withZoneSameInstant(ZoneId.of("UTC"));
        return utcZoned.toLocalDateTime();
    }
}
