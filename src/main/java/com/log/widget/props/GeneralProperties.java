package com.log.widget.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "config")
public class GeneralProperties {

    private String logfilePath;
    private int generationPeriod;

    public String getLogfilePath() {
        return logfilePath;
    }

    public void setLogfilePath(String logfilePath) {
        this.logfilePath = logfilePath;
    }

    public int getGenerationPeriod() {
        return generationPeriod;
    }

    public void setGenerationPeriod(int generationPeriod) {
        this.generationPeriod = generationPeriod;
    }
}


