package com.log.widget.common;

@FunctionalInterface
public interface VoidFunction {
    void execute();
}
