package com.log.widget.common;


public class Constants {
    private Constants(){
        throw new IllegalArgumentException("Constants can't be instantiatsd");
    }

    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss,SSS";
}
