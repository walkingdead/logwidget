package com.log.widget.common;


import com.log.widget.exceptions.FileReadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
@RestController
public class LogWidgetControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogWidgetControllerAdvice.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(FileReadException.class)
    public ErrorEntity handleFileReadException(FileReadException exception) {
        LOGGER.error("Internal server error occurred", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity wrongArgumentType(MethodArgumentTypeMismatchException e) {
        LOGGER.error("Wrong arguments: ", e.getMessage());
        return new ErrorEntity(String.format("Incorrect value [%s] for parameter '%s'", e.getValue(), e.getName()));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity missedInputParameters(MissingServletRequestParameterException exception) {
        String errorMessage = String.format("Required [%s] parameter is missing", exception.getParameterName());
        LOGGER.error("Missing parameters ", errorMessage);
        return new ErrorEntity(errorMessage);
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorEntity handleResourceNotFoundException(Throwable exception) {
        LOGGER.error("Unknown server error occurred", exception);
        return new ErrorEntity("Unknown server error");
    }
}