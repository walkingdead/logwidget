package com.log.widget;

import com.log.widget.props.GeneralProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
public class LogWidgetApplication implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(LogWidgetApplication.class);

    @Autowired
    private GeneralProperties generalProperties;

    public static void main(String[] args) {
        SpringApplication.run(LogWidgetApplication.class);
        logger.info("Application LogWidgetApplication has started");
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        File file = new File(generalProperties.getLogfilePath());
        if (!file.exists()) {
            Path createdFile = Files.createFile(Paths.get(generalProperties.getLogfilePath()));
            logger.info("Log file path " + createdFile.toFile().getAbsolutePath());
        }
    }
}
