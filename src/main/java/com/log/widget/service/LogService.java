package com.log.widget.service;


import com.log.widget.dto.LoggingGeneratorStatus;
import com.log.widget.dto.Statistic;

import java.time.LocalDateTime;

public interface LogService {

    Statistic getLastEvents(LocalDateTime endTime, int period);

    void startLogging();

    void stopLogging();

    LoggingGeneratorStatus getStatus();
}
