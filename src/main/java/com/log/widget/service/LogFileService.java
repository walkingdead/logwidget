package com.log.widget.service;

import com.log.widget.dto.LoggingGeneratorStatus;
import com.log.widget.dto.Statistic;
import com.log.widget.exceptions.FileReadException;
import com.log.widget.model.LogEvent;
import com.log.widget.model.Severity;
import com.log.widget.props.GeneralProperties;
import com.log.widget.utils.CommonUtils;
import com.log.widget.utils.DateTimeUtils;
import org.apache.commons.io.input.ReversedLinesFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;


@Component
public class LogFileService implements LogService {

    private static final Logger logger = LoggerFactory.getLogger(LogFileService.class);
    private static final String GENERAL_MESSAGE = "Some %s message";

    private final File logFile;

    private final ReentrantReadWriteLock logFileLock = new ReentrantReadWriteLock();
    private final ReentrantLock logFileProcessLock = new ReentrantLock();

    private final GeneralProperties generalProperties;
    private final Severity[] severities;
    private final LogEventParser eventParser;

    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> schedule;
    private volatile boolean isStarted;

    @Autowired
    public LogFileService(GeneralProperties generalProperties, LogEventParser eventParser) {
        this.generalProperties = generalProperties;
        this.eventParser = eventParser;
        severities = Severity.values();
        this.logFile = new File(generalProperties.getLogfilePath());
    }

    /**
     * Extract log events statistic for period endDateTime - period up to endDateTime
     * @param endDateTime - end date time of log event
     * @param period - seconds before end date time
     * @return Statistic object according to start and stop date times.
     */
    @Override
    public Statistic getLastEvents(LocalDateTime endDateTime, int period) {
        try {
            logFileLock.readLock().lock();
            LocalDateTime startDateTime = endDateTime.minus(period, ChronoUnit.SECONDS);
            return this.getStatisticFromLogFile(startDateTime, endDateTime);
        } finally {
            logFileLock.readLock().unlock();
        }
    }

    /**
     * Starts generating log events. If already started - do nothing
     */
    @Override
    public void startLogging() {
        CommonUtils.executeWithinLock(this.logFileProcessLock, () -> {
            if (!isStarted) {
                this.schedule = this.scheduler.scheduleAtFixedRate(this::generateLogEvent, 0,
                        this.generalProperties.getGenerationPeriod(), TimeUnit.MILLISECONDS);
                isStarted = true;
                logger.info("Start to generate log events...");
            }
        });
    }

    /**
     * Stops generating log events. If already stopped - do nothing
     */
    @Override
    public void stopLogging() {
        CommonUtils.executeWithinLock(this.logFileProcessLock, () -> {
            if (isStarted) {
                this.schedule.cancel(false);
                isStarted = false;
                logger.info("Stop to generate log events..");
            }
        });
    }

    /**
     * Returns status of logger generator (STARTED or STOPPED)
     *
     * @return LoggingGeneratorStatus - status of logger generator
     */
    @Override
    public LoggingGeneratorStatus getStatus() {
        return this.isStarted ? LoggingGeneratorStatus.STARTED : LoggingGeneratorStatus.STOPPED;
    }

    /**
     * Generates and writes generated events to log file.
     * In case of IOException cancels log generator scheduler.
     */
    private void generateLogEvent() {
        LocalDateTime dateTimeUtc = DateTimeUtils.getLocalDateTimeInUTC(LocalDateTime.now());
        String dateTime = DateTimeUtils.formatDateTime(dateTimeUtc);
        Severity currentSeverity = this.severities[CommonUtils.getRandomIndex(0, this.severities.length)];
        CommonUtils.executeWithinLock(logFileLock.writeLock(), () -> {
            try {
                Files.write(Paths.get(generalProperties.getLogfilePath()),
                        this.buildLogLine(dateTime, currentSeverity).getBytes(), StandardOpenOption.APPEND);
            } catch (IOException ioe) {
                logger.error("Failed to write to log file", ioe);
                schedule.cancel(true);
            }
        });
    }

    /**
     * Retrieves log events from files in range startDateTime - endDateTime
     * In case of invalid log event - logs error to logger
     * If the file can't be read throws custom FileReadException.
     * @param startDateTime - start date time of log event
     * @param endDateTime   - end date time of log event
     * @return Statistic - object that contains statistic in form
     * {"INFO":1,"WARNING":0,"ERROR":1},"status":{"status":"STARTED"}
     * status is needed to keep UI updated to log generator status
     */
    private Statistic getStatisticFromLogFile(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        try (ReversedLinesFileReader reader = new ReversedLinesFileReader(this.logFile, Charset.defaultCharset())) {
            Map<String, Integer> statisticMap = getEmptyStatisticMap();
            String line;
            while ((line = reader.readLine()) != null) {
                try {
                    Optional<LogEvent> logEventOptional = eventParser.parse(line);
                    if(logEventOptional.isPresent()){
                        LogEvent logEvent = logEventOptional.get();
                        if (this.isEventInRange(logEvent, startDateTime, endDateTime)) {
                            Integer count = statisticMap.get(logEvent.getSeverity());
                            if (count != null) {
                                statisticMap.put(logEvent.getSeverity(), count + 1);
                            }
                        }
                        if (logEvent.getDateTime().isBefore(startDateTime)) {
                            break;
                        }
                    }

                } catch (DateTimeParseException dte) {
                    logger.error("Failed to parse logs file event", dte);
                }
            }
            return new Statistic(statisticMap, this.getStatus());
        } catch (IOException io) {
            logger.error("Failed to read logs file", io);
            throw new FileReadException("Failed to read logs file", io);
        }
    }

    /**
     * Verify if log event is in range startDateTime - endDateTime inclusively
     * @param logEvent      - current log event
     * @param startDateTime - start date time of log event
     * @param endDateTime   - end date time of log event
     * @return true if log event in range startDateTime - endDateTime inclusively
     */
    private boolean isEventInRange(LogEvent logEvent, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        LocalDateTime eventDateTime = logEvent.getDateTime();
        return (eventDateTime.isAfter(startDateTime)
                && eventDateTime.isBefore(endDateTime))
                || eventDateTime.equals(startDateTime)
                || eventDateTime.equals(endDateTime);
    }

    /**
     * Builds Log event in format format `<dateTime>` `<severity>` `<message>`
     * @param dateTime - current date time
     * @param severity - randomly selected log event severity
     * @return log event in format `<dateTime>` `<severity>` `<message>`
     */
    private String buildLogLine(String dateTime, Severity severity) {
        return String.format("%s %s %s\n", dateTime, severity.getName(),
                String.format(GENERAL_MESSAGE, severity.getDescription()));
    }

    /**
     * Creates statisticMap with zeros for all severity levels;
     * @return  statisticMap in format {"INFO":0,"WARNING":0,"ERROR":0}
     */
    private Map<String, Integer> getEmptyStatisticMap() {
        return Arrays.stream(this.severities).collect(Collectors.toMap(
                Severity::getName, (severity) -> 0));
    }
}
