package com.log.widget.service;

import com.log.widget.model.LogEvent;
import com.log.widget.model.Severity;
import com.log.widget.utils.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Optional;

@Component
public class LogEventParser {

    private static final Logger logger = LoggerFactory.getLogger(LogEventParser.class);

    public Optional<LogEvent> parse(String event) {
        return this.getEventPart(event);
    }

    private Optional<LogEvent> getEventPart(String event) {

        String[] eventParts = event.split(" ");
        if (eventParts.length < 4) {
            logger.error("Incorrect log event format");
            return Optional.empty();
        }
        String dateTime = String.format("%s %s", eventParts[0], eventParts[1]);
        String severity = eventParts[2];
        String upperCaseSeverity = severity.toUpperCase();
        Optional<Severity> foundSeverity = Arrays.stream(Severity.values()).filter(s -> s.getName().equals(upperCaseSeverity)).findFirst();
        if(!foundSeverity.isPresent()){
            logger.error("Incorrect severity " + severity + " found");
            return Optional.empty();
        }
        StringBuilder message = new StringBuilder();
        for (int i = 3; i < eventParts.length; i++) {
            message.append(eventParts[i]).append(" ");
        }
        return Optional.of(new LogEvent.Builder().setDateTime(DateTimeUtils.parseDateTime(dateTime))
                .setSeverity(upperCaseSeverity)
                .setMessage(message.substring(0, message.length() - 1))
                .build());
    }
}
