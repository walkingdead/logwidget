package com.log.widget.model;


import com.fasterxml.jackson.annotation.JsonFormat;

public enum Severity {
    WARNING("WARNING", "warning"),
    ERROR("ERROR", "error"),
    INFO("INFO", "info"),;

    private final String name;
    private final String description;

    Severity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
