package com.log.widget.model;


import java.time.LocalDateTime;

public class LogEvent {

    private final LocalDateTime dateTime;
    private final String severity;
    private final String message;

    private LogEvent(Builder builder) {
        this.dateTime = builder.dateTime;
        this.severity = builder.severity;
        this.message = builder. message;
    }

    public static class Builder {
        private LocalDateTime dateTime;
        private String severity;
        private String message;

        public Builder setDateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public Builder setSeverity(String severity) {
            this.severity = severity;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public LogEvent build(){
            return new LogEvent(this);
        }
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getSeverity() {
        return severity;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "LogEvent {" +
                "dateTime=" + dateTime +
                ", severity='" + severity + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}


