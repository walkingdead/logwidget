package com.log.widget.controller;

import com.log.widget.common.Constants;
import com.log.widget.dto.LoggingGeneratorStatus;
import com.log.widget.dto.Statistic;
import com.log.widget.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class LogWidgetController {

    private static final Logger logger = LoggerFactory.getLogger(LogWidgetController.class);

    private final LogService logService;

    @Autowired
    public LogWidgetController(LogService logService) {
        this.logService = logService;
    }

    @PutMapping("start")
    public ResponseEntity<LoggingGeneratorStatus> startLogging() {
        logger.info("Start event generating");
        this.logService.startLogging();
        return ResponseEntity.ok(this.logService.getStatus());
    }

    @PutMapping("stop")
    public ResponseEntity<LoggingGeneratorStatus> stopLogging() {
        logger.info("Stop event generating");
        this.logService.stopLogging();
        return ResponseEntity.ok(this.logService.getStatus());
    }

    @GetMapping("status")
    public ResponseEntity<LoggingGeneratorStatus> loggingStatus() {
        logger.info("Get logging status");
        return ResponseEntity.ok(this.logService.getStatus());
    }

    @GetMapping("statistic")
    public ResponseEntity getStatistic(@RequestParam("endDateTime")
                                       @DateTimeFormat(pattern = Constants.DATE_TIME_PATTERN) LocalDateTime endDateTime,
                                       @RequestParam("period") int period) {
        Statistic lastEvents = this.logService.getLastEvents(endDateTime, period);
        return ResponseEntity.ok(lastEvents);
    }

}
