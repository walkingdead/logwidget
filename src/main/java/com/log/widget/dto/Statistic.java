package com.log.widget.dto;


import java.util.Map;

public class Statistic {

    private Map<String, Integer> logStatistic;
    private LoggingGeneratorStatus status;

    public Statistic(Map<String, Integer> logStatistic, LoggingGeneratorStatus status) {
        this.logStatistic = logStatistic;
        this.status = status;
    }

    public Statistic() {
    }

    public Map<String, Integer> getLogStatistic() {
        return logStatistic;
    }

    public void setLogStatistic(Map<String, Integer> logStatistic) {
        this.logStatistic = logStatistic;
    }

    public LoggingGeneratorStatus getStatus() {
        return status;
    }

    public void setStatus(LoggingGeneratorStatus status) {
        this.status = status;
    }
}
