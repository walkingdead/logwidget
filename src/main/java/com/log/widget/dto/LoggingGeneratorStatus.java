package com.log.widget.dto;


import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LoggingGeneratorStatus {

    STARTED("STARTED"),
    STOPPED("STOPPED");

    private final String status;

    private LoggingGeneratorStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
